package com.example;


import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import java.util.AbstractMap.SimpleImmutableEntry;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@Test
public class DriverTest {

    @Test
    public void basicTest() {
        // given
        OutInterface outInterface = Mockito.mock(OutInterface.class);
        InInterface inInterface = new InInterfaceImpl();
        Driver driver = new Driver(inInterface, outInterface);

        // when
        when(outInterface.getCurrentPosition()).thenReturn(new SimpleImmutableEntry<Integer, Integer>(0, 0));
        inInterface.internalKeypad(2);
        driver.doLogic();

        // then
        Mockito.verify(outInterface, times(1)).goToLevel(Matchers.eq(2));
    }

    @Test
    public void testDoNothing() {
        // given
        OutInterface outInterface = Mockito.mock(OutInterface.class);
        InInterface inInterface = new InInterfaceImpl();
        Driver driver = new Driver(inInterface, outInterface);

        // when
        when(outInterface.getCurrentPosition()).thenReturn(new SimpleImmutableEntry<Integer, Integer>(0, 0));
        inInterface.internalKeypad(0);
        driver.doLogic();

        // then
        Mockito.verify(outInterface, never()).goToLevel(anyInt());
    }

    @Test
    public void testSequence() {
        // given
        OutInterface outInterface = Mockito.mock(OutInterface.class);
        InInterface inInterface = new InInterfaceImpl();
        Driver driver = new Driver(inInterface, outInterface);

        // when
        when(outInterface.getCurrentPosition()).thenReturn(new SimpleImmutableEntry<Integer, Integer>(0, 0));
        inInterface.internalKeypad(3);
        inInterface.internalKeypad(2);
        inInterface.internalKeypad(1);
        driver.doLogic();

        // then
        Mockito.verify(outInterface, times(1)).goToLevel(eq(1));
        Mockito.verify(outInterface, times(1)).goToLevel(eq(2));
        Mockito.verify(outInterface, times(1)).goToLevel(eq(3));
    }

    @Test
    public void testCorrectSequence() {
        // given
        OutInterface outInterface = Mockito.mock(OutInterface.class);
        InInterface inInterface = new InInterfaceImpl();
        Driver driver = new Driver(inInterface, outInterface);

        // when
        when(outInterface.getCurrentPosition()).thenReturn(new SimpleImmutableEntry<Integer, Integer>(0, 0));
        inInterface.internalKeypad(3);
        inInterface.internalKeypad(2);
        inInterface.internalKeypad(1);
        driver.doLogic();

        // then
        InOrder inOrder = Mockito.inOrder(outInterface);
        Mockito.verify(outInterface, times(1)).goToLevel(eq(1));
        Mockito.verify(outInterface, times(1)).goToLevel(eq(2));
        Mockito.verify(outInterface, times(1)).goToLevel(eq(3));
    }
}