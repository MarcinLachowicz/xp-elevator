package com.example;


import java.util.Iterator;
import java.util.Map;

public class Driver {

    private InInterface inInterface;
    private OutInterface outInterface;

    public Driver(InInterface inInterface, OutInterface outInterface) {
        this.inInterface = inInterface;
        this.outInterface = outInterface;
    }


    public void doLogic() {
        Iterator<Integer> levelsIterator = inInterface.getLevelsIterator();
        while (levelsIterator.hasNext()) {
            int targetLevel = levelsIterator.next();
            Map.Entry<Integer, Integer> currentPosition = outInterface.getCurrentPosition();
            int toPosition = currentPosition.getValue();
            if (toPosition != targetLevel) {
                outInterface.goToLevel(targetLevel);
            }
        }
    }
}
