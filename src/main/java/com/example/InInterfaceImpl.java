package com.example;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Marcin Lachowicz (SG0221143)
 *         <p>
 *         Copyright (c) 2016 Sabre Holdings, Inc. All Rights Reserved.
 */
public class InInterfaceImpl implements InInterface {

    private Queue<Integer> levels = new LinkedList<Integer>();

    public void externalStop(int level) {
        levels.offer(level);
    }

    public void internalKeypad(int level) {
        levels.offer(level);
    }

    public Iterator<Integer> getLevelsIterator() {
        return levels.iterator();
    }


}
