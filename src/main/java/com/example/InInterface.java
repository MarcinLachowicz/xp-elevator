package com.example;


import java.util.Iterator;

public interface InInterface {
    void externalStop(int level);

    void internalKeypad(int level);

    Iterator<Integer> getLevelsIterator();
}
