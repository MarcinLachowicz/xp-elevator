package com.example;

import java.util.Map;

public interface OutInterface {
    void goToLevel(int level);

    Map.Entry<Integer, Integer> getCurrentPosition();

    void emergencyStop();
}
